<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan_model extends CI_Model {

    public function addKecamatan(){
        $data = [
            "id_kabupaten" => $this->input->post('kabupaten', 'true'),
            "nama_kecamatan" => $this->input->post('kecamatan', 'true')
        ];
        $this->db->insert("kecamatan", $data);
    }

    public function getKecamatan(){
        $this->db->select('kota_kabupaten.*, kecamatan.*');
        $this->db->from('kota_kabupaten');
        $this->db->join('kecamatan', 'kota_kabupaten.id=kecamatan.id_kabupaten');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete($id){
        $this->db->delete('kecamatan', ['id_kecamatan' => $id]);
    }

    public function show($id){
        return $this->db->get_where('kecamatan', ['id_kecamatan' => $id])->result_array()[0];
    }

    public function showKecamatan($id){
        return $this->db->get_where('kecamatan', ['id_kabupaten' => $id])->result_array();
    }

    public function edit($id)
    {
        $data = [
            "id_kabupaten" => $this->input->post('kabupaten', 'true'),
            "nama_kecamatan" => $this->input->post('kecamatan', 'true')
        ];

        $this->db->where('id_kecamatan', $id);
        $this->db->update('kecamatan', $data);
    }
}