<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten_model extends CI_Model {

    public function addKabupaten(){
        $data = [
            "nama" => $this->input->post('kabupaten', 'true')
        ];
        $this->db->insert("kota_kabupaten", $data);
    }

    public function getKabupaten(){
        return $this->db->get('kota_kabupaten')->result_array();
    }

    public function delete($id){
        $this->db->delete('kota_kabupaten', ['id' => $id]);
    }

    public function show($id){
        return $this->db->get_where('kota_kabupaten', ['id' => $id])->result_array()[0];
    }

    public function edit($id)
    {
        $data = [
            "nama" => $this->input->post('kabupaten', 'true')
        ];

        $this->db->where('id', $id);
        $this->db->update('kota_kabupaten', $data);
    }
}