<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa_model extends CI_Model {

    public function addSiswa(){
        $data = [
            "id_kecamatan" => $this->input->post('kecamatan', 'true'),
            "nama_siswa" => $this->input->post('siswa', 'true'),
            "alamat" => $this->input->post('alamat', 'true')
        ];
        $this->db->insert("siswa", $data);
    }

    public function getSiswa(){
        $this->db->from('kecamatan');
        $this->db->join('siswa', 'kecamatan.id_kecamatan=siswa.id_kecamatan');
        $this->db->join('kota_kabupaten', 'kota_kabupaten.id=kecamatan.id_kabupaten');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete($id){
        $this->db->delete('siswa', ['id_siswa' => $id]);
    }

    public function show($id){
        $this->db->from('kecamatan');
        $this->db->join('siswa', 'kecamatan.id_kecamatan=siswa.id_kecamatan');
        $this->db->where('id_siswa', $id);
        $query = $this->db->get();
        return $query->result_array()[0];
    }

    public function edit($id)
    {
        $data = [
            "id_kecamatan" => $this->input->post('kecamatan', 'true'),
            "nama_siswa" => $this->input->post('siswa', 'true'),
            "alamat" => $this->input->post('alamat', 'true')
        ];

        $this->db->where('id_siswa', $id);
        $this->db->update('siswa', $data);
    }
}