<div class="col-lg-12 col-md-12 col-12 mb-4 pr-0 pl-0">
<!-- Project Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-11">
                    <h6 class="m-0 font-weight-bold text-primary">Kabupaten</h6>
                </div>
                <div class="col-1">
                <a href="#" onclick="add()" class="badge badge-dark" >Tambah</a>
                </div>
            </div>
        </div>
        <div class="card-body">
<?php if($this->session->flashdata('flash')) : ?>
        <div class="row">
            <div class="col-6">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?= $this->session->flashdata('flash') ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
<?php endif; ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Kabupaten</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
        <?php foreach($kabupaten as $row){ ?>
                    <tr>
                        <td><?= $row['nama'] ?></td>
                        <td>
                        <a href="#" onclick=edit(<?= $row['id'] ?>) class="badge badge-success">edit</a>
                        <a href="<?= base_url(); ?>kabupaten/delete/<?= $row['id'] ?>" class="badge badge-danger" onclick="return confirm('Yakin?')">hapus</a>
                        </td>
                    </tr>
        <?php } ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <form id="form_type" action="" method="POST">
            <div class="modal-content animated fadeInUp">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Kabupaten</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="kabupaten">Nama Kabupaten/Kota</label>
                                <input type="text" class="form-control" placeholder="Masukan nama kabupaten atau kota" name="kabupaten" id="kabupaten" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><span id="submit"><i class="fa fa-plus"></i> Tambah Kabupaten</span> </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    function add() {
        $("#modal").modal('show');
        $('#form_type').attr('action', "<?= base_url('kabupaten/add') ?>");
        $('.modal-title').text('Tambah Kabupaten');
        $('#kabupaten').val('');
        $('#submit').text('Tambah')
    }

    function edit(id) {
        $("#modal").modal('show');
        $('#form_type').attr('action', "<?= base_url('kabupaten/edit/') ?>" + id);
        $('.modal-title').text('Edit Kabupaten');
        $('#submit').text('Edit')
        $.ajax({
            url: "<?= base_url('kabupaten/show/') ?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                $('#kabupaten').val(data.nama);
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
            }
        });
    }
    </script>