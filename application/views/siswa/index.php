<div class="col-lg-12 col-md-12 col-12 mb-4 pr-0 pl-0">
<!-- Project Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-11">
                    <h6 class="m-0 font-weight-bold text-primary">Siswa</h6>
                </div>
                <div class="col-1">
                <a href="#" onclick="add()" class="badge badge-dark" >Tambah</a>
                </div>
            </div>
        </div>
        <div class="card-body">
<?php if($this->session->flashdata('flash')) : ?>
        <div class="row">
            <div class="col-6">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?= $this->session->flashdata('flash') ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
<?php endif; ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nama Siswa</th>
                        <th>Kota/Kabupaten</th>
                        <th>Kecamatan</th>
                        <th>Alamat</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
        <?php 
            foreach($siswa as $row){
        ?>
                    <tr>
                        <td><?= $row['nama_siswa'] ?></td>
                        <td><?= $row['nama'] ?></td>
                        <td><?= $row['nama_kecamatan'] ?></td>
                        <td><?= $row['alamat'] ?></td>
                        <td>
                        <a href="#" onclick=edit(<?= $row['id_siswa'] ?>) class="badge badge-success">edit</a>
                        <a href="<?= base_url(); ?>siswa/delete/<?= $row['id_siswa'] ?>" class="badge badge-danger" onclick="return confirm('Yakin?')">hapus</a>
                        </td>
                    </tr>
        <?php
            }
        ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <form id="form_type" action="" method="POST">
            <div class="modal-content animated fadeInUp">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Kecamatan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="siswa">Nama Siswa</label>
                                <input type="text" class="form-control" placeholder="Masukan nama siswa" name="siswa" id="siswa" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="kabupaten">Kabupaten</label>
                                <select id="kabupaten" class="form-control">
                                    <option value="" disabled="disabled">--pilih Kabupaten--</option>
                                    <?php
                                    foreach ($kabupatens as $kabupaten) {
                                    ?>
                                            <option value="<?= $kabupaten['id'] ?>"><?= $kabupaten['nama'] ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kecamatan">Kecamatan</label>
                                <select id="kecamatan" name="kecamatan" class="form-control">
                                    <option value="" disabled="disabled" selected="selected">--pilih kabupaten terlebih dahulu--</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <textarea class="form-control" id="alamat" rows="3" autocomplete="off" name="alamat" placeholder="masukan alamat siswa"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><span id="submit"><i class="fa fa-plus"></i> Tambah</span> </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<?= base_url() ?>asset/vendor/jquery/jquery.js"></script>
<script>
$(document).ready(function(){
    $("#kabupaten").on('change', function() {
        $.ajax({
            url: "<?= base_url('Kecamatan/showKecamatan/') ?>" + this.value,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data)
                var i = 0;
                $('#kecamatan').html('');
                $('#kecamatan').append(
                    $("<option value=''>--pilih kecamatan--</option>")
                );
                $.each(
                    data,
                    function(intIndex, objValue) {
                        $('#kecamatan').append(
                            $("<option value=" + data[i]['id_kecamatan'] + ">" + data[i]['nama_kecamatan'] + "</option>")
                        );
                        i++;
                    }
                );
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
            }
        });
    });
});

    function add() {
        $("#modal").modal('show');
        $('#form_type').attr('action', "<?= base_url('siswa/add') ?>");
        $('.modal-title').text('Tambah Siswa');
        $('#siswa').val('');
        $('#kecamatan').val('');
        $('#kabupaten').val('');
        $('#alamat').val('');
        $('#submit').text('Tambah')
    }

    function edit(id) {
        $("#modal").modal('show');
        $('#form_type').attr('action', "<?= base_url('siswa/edit/') ?>" + id);
        $('.modal-title').text('Edit Siswa');
        $('#submit').text('Edit')
        $.ajax({
            url: "<?= base_url('siswa/show/') ?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                var id_kec = data.id_kecamatan;
                console.log(id_kec);
                $('#siswa').val(data.nama_siswa);
                $('#alamat').val(data.alamat);
                $.ajax({
                    url: "<?= base_url('Kecamatan/showKecamatan/') ?>" + data.id_kabupaten,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data) {
                        var i = 0;
                        $('#kecamatan').html('');
                        $('#kecamatan').append(
                            $("<option value=''>--pilih kecamatan--</option>")
                        );
                        $.each(
                            data,
                            function(intIndex, objValue) {
                                if(data[i]['id_kecamatan'] == id_kec){
                                    $('#kecamatan').append(
                                        $("<option selected value=" + data[i]['id_kecamatan'] + ">" + data[i]['nama_kecamatan'] + "</option>")
                                    );
                                }else{
                                    $('#kecamatan').append(
                                        $("<option value=" + data[i]['id_kecamatan'] + ">" + data[i]['nama_kecamatan'] + "</option>")
                                    );
                                }
                                i++;
                            }
                        );
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                    }
                });
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
            }
        });
    }
    </script>