<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kecamatan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kecamatan_model');
        $this->load->model('kabupaten_model');
    }

    public function index()
    {
        $data['judul'] = "Kelola Kecamatan";
        $data['kecamatan'] = $this->kecamatan_model->getKecamatan();
        $data['kabupatens'] = $this->kabupaten_model->getKabupaten();

		$this->load->view('template/heading', $data);
		$this->load->view('kecamatan/index', $data);
		$this->load->view('template/footer');
    }

    public function add()
    {
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('flash', 'Data Kecamatan tidak berhasil ditambah.');
            redirect('kecamatan');
        } else {
            $this->kecamatan_model->addKecamatan();
            $this->session->set_flashdata('flash', 'Data Kecamatan berhasil ditambah.');
            redirect('kecamatan');
        }
    }

    public function edit($id)
    {
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('flash', 'Data Kecamatan tidak berhasil diedit.');
            redirect('kecamatan');
        } else {
            $this->kecamatan_model->edit($id);
            $this->session->set_flashdata('flash', 'Data Kecamatan berhasil diedit.');
            redirect('kecamatan');
        }
    }

    public function show($id)
    {
        $data = $this->kecamatan_model->show($id);
        echo json_encode($data);
    }

    public function showKecamatan($id){
        $data = $this->kecamatan_model->showKecamatan($id);
        echo json_encode($data);
    }

    public function delete($id)
    {
        $this->kecamatan_model->delete($id);
        $this->session->set_flashdata('flash', 'Data kecamatan berhasil dihapus.');
        redirect('kecamatan');
    }
}
