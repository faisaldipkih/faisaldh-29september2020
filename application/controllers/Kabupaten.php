<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kabupaten extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kabupaten_model');
    }

    public function index()
    {
        $data['judul'] = "Kelola Kabupaten";
        $data['kabupaten'] = $this->kabupaten_model->getKabupaten();

		$this->load->view('template/heading', $data);
		$this->load->view('kabupaten/index', $data);
		$this->load->view('template/footer');
    }

    public function add()
    {
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('flash', 'Data Kota/Kabupaten tidak berhasil ditambah.');
            redirect('kabupaten');
        } else {
            $this->kabupaten_model->addKabupaten();
            $this->session->set_flashdata('flash', 'Data Kota/Kabupaten berhasil ditambah.');
            redirect('kabupaten');
        }
    }

    public function edit($id)
    {
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('flash', 'Data Kota/Kabupaten tidak berhasil diedit.');
            redirect('kabupaten');
        } else {
            $this->kabupaten_model->edit($id);
            $this->session->set_flashdata('flash', 'Data Kota/Kabupaten berhasil diedit.');
            redirect('kabupaten');
        }
    }

    public function show($id)
    {
        $data = $this->kabupaten_model->show($id);
        echo json_encode($data);
    }

    public function delete($id)
    {
        $this->kabupaten_model->delete($id);
        $this->session->set_flashdata('flash', 'Data Kota/Kabupaten berhasil dihapus.');
        redirect('kabupaten');
    }
}
