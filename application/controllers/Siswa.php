<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kecamatan_model');
        $this->load->model('kabupaten_model');
        $this->load->model('siswa_model');
    }

    public function index()
    {
        $data['judul'] = "Kelola Siswa";
        $data['kecamatan'] = $this->kecamatan_model->getKecamatan();
        $data['kabupatens'] = $this->kabupaten_model->getKabupaten();
        $data['siswa'] = $this->siswa_model->getSiswa();

		$this->load->view('template/heading', $data);
		$this->load->view('siswa/index', $data);
		$this->load->view('template/footer');
    }

    public function add()
    {
        $this->form_validation->set_rules('siswa', 'Siswa', 'required');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('flash', 'Data Siswa tidak berhasil ditambah.');
            redirect('siswa');
        } else {
            $this->siswa_model->addSiswa();
            $this->session->set_flashdata('flash', 'Data Siswa berhasil ditambah.');
            redirect('siswa');
        }
    }

    public function edit($id)
    {
        $this->form_validation->set_rules('siswa', 'Siswa', 'required');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('flash', 'Data Siswa tidak berhasil diedit.');
            redirect('siswa');
        } else {
            $this->siswa_model->edit($id);
            $this->session->set_flashdata('flash', 'Data Siswa berhasil diedit.');
            redirect('siswa');
        }
    }

    public function show($id)
    {
        $data = $this->siswa_model->show($id);
        echo json_encode($data);
    }

    public function delete($id)
    {
        $this->siswa_model->delete($id);
        $this->session->set_flashdata('flash', 'Data siswa berhasil dihapus.');
        redirect('siswa');
    }
}
