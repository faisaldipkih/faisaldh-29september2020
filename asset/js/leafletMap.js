var mymap = L.map('mapid').setView([-7.3770295, 107.820542], 10);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZmFpc2FsZGlwa2loIiwiYSI6ImNrYjh0ZWhpeDAxbmMycG1uOW85dnVkbHIifQ.rKbB6o93q4MDHZ1F179MRg', {
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	maxZoom: 18,
	id: 'mapbox/streets-v11',
	tileSize: 512,
	zoomOffset: -1,
	accessToken: 'your.mapbox.access.token'
}).addTo(mymap);

var myStyle = {
	"color": "#ff7800",
	"weight": 5,
	"opacity": 0.65
};

function popUp(f, l) {
	var out = [];
	if (f.properties) {
		for (key in f.properties) {
			out.push(key + ": " + f.properties[key]);
		}
		l.bindPopup(out.join("<br />"));
	}
}
var jsonTest = new L.GeoJSON.AJAX(["uploads/geoJSon/bungbulang.geojson"], {
	onEachFeature: popUp,
	style: myStyle
}).addTo(mymap);

// var marker = L.marker([-7.0743247, 107.8782323]).addTo(mymap).bindPopup("<b>Hello world!</b><br />I am a popup.").openPopup();
